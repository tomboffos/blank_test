<?php


namespace App\Contracts;


interface ProductContract
{
    const NAME = 'name';
    const DESCRIPTION = 'description';
    const PRICE = 'price';
    const PUBLISHED = 'published';
    const IMAGE = 'image';

    const FILLABLE = [
        self::NAME,
        self::DESCRIPTION,
        self::PRICE,
        self::PUBLISHED,
        self::IMAGE
    ];


}
