<?php


namespace App\Contracts;


interface CategoryContract
{
    const NAME = 'name';
    const DESCRIPTION = 'description';

    const FILLABLE = [
        self::NAME,
        self::DESCRIPTION
    ];
}
