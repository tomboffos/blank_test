<?php


namespace App\Repositories\Interfaces;


use App\Http\Requests\Api\Products\StoreProductRequest;
use App\Http\Requests\Api\Products\UpdateProductRequest;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

interface ProductRepositoryInterface
{
    public function listProduct(Request $request);

    public function storeProduct(StoreProductRequest $request);

    public function updateProduct(UpdateProductRequest $request, Product $product);

    public function showProduct(Product $product);

    public function deleteProduct(Product $product);

    public function attachCategoryToProduct(Category $category, Product $product);

    public function detachCategoryFromProduct(Category $category, Product $product);
}
