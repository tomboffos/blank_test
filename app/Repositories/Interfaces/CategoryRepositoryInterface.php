<?php


namespace App\Repositories\Interfaces;


use App\Http\Requests\Api\Categories\StoreCategoryRequest;
use App\Models\Category;
use App\Models\Product;

interface CategoryRepositoryInterface
{
    public function storeCategory(StoreCategoryRequest $request);

    public function deleteCategory(Category $category);
}
