<?php


namespace App\Repositories;


use App\Contracts\CategoryContract;
use App\Contracts\ProductCategoryContract;
use App\Contracts\ProductContract;
use App\Http\Requests\Api\Products\StoreProductRequest;
use App\Http\Requests\Api\Products\UpdateProductRequest;
use App\Http\Resources\Products\ProductResource;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Repositories\Interfaces\ProductRepositoryInterface;
use Illuminate\Http\Request;

class ProductRepository implements ProductRepositoryInterface
{
    public function listProduct(Request $request)
    {
        return ProductResource::collection(
            Product::where(function ($query) use ($request) {
                if ($request->has('product_name'))
                    $query->where(ProductContract::NAME, 'iLIKE', '%' . $request->product_name . '%');
                if ($request->has('category_name'))
                    $query->whereHas('productsCategories', function ($secondQuery) use ($request) {
                        $secondQuery->where(CategoryContract::NAME, 'iLIKE', '%' . $request->categoryName . '%');
                    });
                if ($request->has('published'))
                    $query->where(ProductContract::PUBLISHED, $request->published);
                if ($request->has('min_price') && $request->has('max_price'))
                    $query->whereBetween(ProductContract::PRICE, [$request->min_price, $request->max_price]);
            })->paginate(10)
        );
    }

    public function storeProduct(StoreProductRequest $request)
    {
        $product = Product::create($request->validated());

        foreach ($request->category_ids as $category_id)
            $this->attachCategoryToProduct(Category::find($category_id), $product);

        return new ProductResource($product);
    }

    public function updateProduct(UpdateProductRequest $request, Product $product)
    {
        $product->update($request->validated());

        return new ProductResource($product);
    }

    public function showProduct(Product $product)
    {
        return new ProductResource($product);
    }

    public function deleteProduct(Product $product)
    {
        $product->delete();

        return response([
            'message' => 'Product successfully deleted!'
        ], 200);
    }

    public function attachCategoryToProduct(Category $category, Product $product)
    {
        if (!ProductCategory::where(ProductCategoryContract::CATEGORY_ID, $category->id)->where(ProductCategoryContract::PRODUCT_ID, $product->id)->exists())
            $product->productsCategories()->attach($category->id);

        return response([
            'message' => 'Category added to product'
        ], 200);
    }

    public function detachCategoryFromProduct(Category $category, Product $product)
    {
        if (ProductCategory::where(ProductCategoryContract::CATEGORY_ID, $category->id)->where(ProductCategoryContract::PRODUCT_ID, $product->id)->exists())
            $product->productsCategories()->detach($category->id);

        return response([
            'message' => 'Category removed from product'
        ], 200);
    }
}
