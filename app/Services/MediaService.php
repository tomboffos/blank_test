<?php


namespace App\Services;


use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Nette\Utils\Image;

class MediaService
{
    public static function processImage($value)
    {
        return asset('/storage/'.str_replace('public/','',$value->store('public/avatars')));
    }
}
