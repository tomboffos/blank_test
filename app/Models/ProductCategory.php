<?php

namespace App\Models;

use App\Contracts\ProductCategoryContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductCategory extends Model
{
    use HasFactory;

    protected $fillable = ProductCategoryContract::FILLABLE;
}
