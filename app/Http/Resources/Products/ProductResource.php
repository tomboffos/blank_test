<?php

namespace App\Http\Resources\Products;

use App\Contracts\ProductContract;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            ProductContract::NAME => $this->name,
            ProductContract::IMAGE => $this->image,
            ProductContract::PUBLISHED => $this->published,
            ProductContract::PRICE => $this->price,
            ProductContract::DESCRIPTION => $this->description,
            'categories' => $this->productsCategories
        ];
    }
}
