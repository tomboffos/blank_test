<?php

namespace App\Http\Requests\Api\Categories;

use App\Contracts\CategoryContract;
use Illuminate\Foundation\Http\FormRequest;

class StoreCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            CategoryContract::NAME => 'required',
            CategoryContract::DESCRIPTION => 'required'
        ];
    }
}
