<?php

namespace App\Http\Requests\Api\Products;

use App\Contracts\ProductContract;
use Illuminate\Foundation\Http\FormRequest;

class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            ProductContract::NAME => 'required',
            ProductContract::DESCRIPTION => 'required',
            ProductContract::IMAGE => 'required',
            ProductContract::PUBLISHED => 'boolean|required',
            ProductContract::PRICE => 'required',
            'category_ids' => 'required|array|min:2|max:10',
        ];
    }
}
