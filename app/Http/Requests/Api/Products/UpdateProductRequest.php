<?php

namespace App\Http\Requests\Api\Products;

use App\Contracts\ProductContract;
use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            ProductContract::NAME => 'nullable',
            ProductContract::DESCRIPTION => 'nullable',
            ProductContract::IMAGE => 'nullable',
            ProductContract::PUBLISHED => 'boolean|nullable',
            ProductContract::PRICE => 'nullable'
        ];
    }
}
