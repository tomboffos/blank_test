<?php

namespace App\Http\Controllers\Api\Products;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Products\StoreProductRequest;
use App\Http\Requests\Api\Products\UpdateProductRequest;
use App\Models\Category;
use App\Models\Product;
use App\Repositories\Interfaces\ProductRepositoryInterface;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    protected $productRepository;

    public function __construct(ProductRepositoryInterface $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->productRepository->listProduct($request);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreProductRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductRequest $request)
    {
        return $this->productRepository->storeProduct($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return $this->productRepository->showProduct($product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateProductRequest  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductRequest $request, Product $product)
    {
        return $this->productRepository->updateProduct($request, $product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $this->productRepository->deleteProduct($product);
    }

    /**
     * Attach the specified resource to another in storage.
     *
     * @param Category $category
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function attach(Category $category, Product $product)
    {
        return $this->productRepository->attachCategoryToProduct($category, $product);
    }

    /**
     * Detach the specified resource from another in storage.
     *
     * @param Category $category
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function detach(Category $category, Product $product)
    {
        return $this->productRepository->detachCategoryFromProduct($category, $product);
    }
}
